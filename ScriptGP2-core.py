#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-core.py
# Versao: 
#
# Função: O programa recebe duas lista de idlattes, uma do curriculo lattes e outra do diretorio de grupos de pesquisa
#         Com base nas duas listas o programa identifica os idlattes em comum, ou seja core competences
#           
# Programador: André Santos 
#
# Uso: ScriptGP2-core.py <arquivo de configuracao)
# 
# <arquivo de configuração> 
# O arquivo de configuracao deve conter os seguintes parametros
# listascriptlattes = <arquivo listas>
#                   local do arquivo que contem a lista com os idlattes do ScriptLattes
# listascriptgp = <arquivo listas>
#                   local do arquivo que contem a lista com os idlattes do ScriptGP
# arquivosaida = nome do arquivo de saida
# exemplo (veja o ScriptGP2-core.config)

#=============================================================
#SETTINGS
#=============================================================

#caminho e prefixo para o arquivo de saída
#oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-final-'
#arquivolistas = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\arquivos_nano_para_juntar.txt"

import unicodedata
import sys
def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
#=============================================================
#MAIN PROGRAM
#=============================================================

#le o arquivo de configuracao
arquivoConfig = sys.argv[1]
#arquivoConfig = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\scriptgp-core.config"
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
        
listatotal = []
arquivoScriptLattes = opcoes['listascriptlattes']
arquivoScriptGP = opcoes['listascriptgp']
oufile = opcoes['arquivosaida']
listaIdlattesScriptLattes = []
listaIdlattesScriptGP = []
listaIdgpIdlattesCore = []
dictIdlattesIdgp = dict()

#le o arquivo com os arquivos de listas
with open(arquivoScriptLattes, 'r') as fi1:
    for k in fi1:
        listaIdlattesScriptLattes.append(k.split(',')[0].strip())
with open(arquivoScriptGP, 'r') as fi1:
    for k in fi1:
        idlattes = k.split('\t')[1].strip()
        listaIdlattesScriptGP.append(k.split('\t')[1].strip())
        dictIdlattesIdgp[idlattes] = k

listaIdlattesScriptLattes = set(listaIdlattesScriptLattes)
listaIdlattesScriptGP = set(listaIdlattesScriptGP)
listaIdlattesCore = listaIdlattesScriptLattes & listaIdlattesScriptGP
for k in listaIdlattesCore:
    listaIdgpIdlattesCore.append(dictIdlattesIdgp[k].strip())
    
print 'Total de pesquisadores ScriptLattes..: ' + str(len(listaIdlattesScriptLattes))
print 'Total de pesquisadores ScriptGP......: ' + str(len(listaIdlattesScriptGP))
print 'Total de pesquisadores Core Compet...: ' + str(len(listaIdlattesCore))
with open(oufile+'idlattes-core.list', 'w') as fo:
    for linhas in listaIdlattesCore:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idgps-idlattes-core.list', 'w') as fo:
    for linhas in listaIdgpIdlattesCore:
        fo.write("%s\n" % linhas)
