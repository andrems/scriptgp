ScriptGP2

Manual de Instalação e Uso
2016, Summer Version


O que é o ScritpGP2

O ScriptGP2 é um conjunto de programas que permitem realizar buscas, extrair e processar informações sobre os grupos de pesquisa do Diretório de Grupos de Pesquisa do CNPQ.
Para entender o funcionamento do ScriptGP é preciso conhecer as duas bases principais da plataforma Lattes do CNPQ: 
•	Currículo Lattes (http://lattes.cnpq.br)
•	Grupos de Pesquisa (http://lattes.cnpq.br/web/dgp)
A base Currículo Lattes abriga todas as informações dos pesquisadores cadastrados no CNPQ. Cada pesquisador é identificado por um código único de 16 dígitos, que escolhemos chamar de IDLATTES. A análise dos dados de pesquisadores pode ser feita através de outro programa, o ScriptLattes (url). Com o ScriptLattes é possível descobrir, por exemplo, todos os pesquisadores que atuam em determinado tema ou linha de pesquisa. 
A base de Grupos de Pesquisa abriga as informações de todos os grupos de pesquisa cadastrados no CNPQ. O cadastro permite registrar informações das áreas de atuação, linhas de pesquisa, pesquisadores, estudantes, técnicos, instituições, equipamentos e outros dados sobre um grupo de pesquisa. Assim como nos pesquisadores, cada grupo possui um código único de 16 dígitos que escolhemos chamar de IDGP. Com o pacote de programas do ScriptGP2 é possível extrair todas as informações dos grupos de pesquisa cadastrados na base. 

O ScriptGP2 é composto por 8 programas
•	ScriptGP2-busca
•	ScriptGP2-grupos
•	ScriptGP2-lattes
•	ScriptGP2-pesquisadores
•	ScriptGP2-estudantes
•	ScriptGP2-instituicoes
•	ScriptGP2-join
•	ScriptGP2-cleaner


ScriptGP2-busca realiza uma busca por palavras-chave na plataforma de grupos de pesquisa do CNPQ e retorna o IDGP de cada grupo encontrado. O IDGP é um identificador único para cada grupo de pesquisa e permite recuperar todas as informações do grupo na base do CNPQ através do ScriptGP2-cadastro, ScriptGP2-pesquisadores, ScriptGP2-estudantes e ScriptGP2-instituicoes.

ScriptGP2-lattes permite obter uma lista dos grupos de pesquisa dos quais um ou mais pesquisadores pertencem. A partir de um conjunto de IDLATTES (identificador único de cada pesquisador na plataforma curricular Lattes), o ScriptGP2-lattes retorna a lista com o IDGP de todos os grupos relacionados. O ScriptGP-lattes também gera um arquivo de grafo contendo pares de nós representados pelo pesquisador e o grupo ao qual pertence. Desta foram é possível construir gráficos de redes que representem a ligação entre grupos de pesquisa por meio dos pesquisadores em comum. A lista de IDGP também pode ser utilizada para extrair qualquer uma das informações dos grupos, através dos scripts ScriptGP2-grupos, ScriptGP2-pesquisadores, ScriptGP2-estudantes e ScriptGP2-instituicoes.

ScriptGP2-cadastro permite recuperar os principais dados cadastrais dos grupos como, por exemplo, ano de formação, instituição, áreas de conhecimento, cidade, UF, e dados do número de pesquisadores, estudantes e técnicos que pertencem ao grupo. O Script usa como base uma lista de IDGP, obtida por meio da ScriptGP2-busca ou ScriptGP2-lattes para acessar os grupos e extrair as informações. O programa também gera uma página html com todos os dados no formato de uma Pivot Table, permitindo a análise e cruzamento dos dados. 

ScriptGP2-pesquisadores permite recuperar o IDLATTES e nome de todos os pesquisadores pertencentes a um grupo de pesquisa a partir de uma lista de IDGPs. O script também gera uma pivottable com os grupos e pesquisadores.

ScriptGP2-estudantes permite recuperar o IDLATTES e nome de todos os estudantes pertencentes a um grupo de pesquisa a partir de uma lista de IDGPs. O script também gera uma pivottable com os grupos e pesquisadores.

ScriptGP2-instituicoes permite recuperar as instituições (empresas ou outras organizações) que possuem alguma parceria com os grupos de pesquisa a partir de uma lista de IDGPs. O script recupera o nome, razão social, cnpj, sigla, endereço, cidade, UF, natureza jurídica e setor de atuação das empresas.

ScriptGP2-join é um programa que permite juntar diversas listas de IDGPs em um único arquivo. O usuário pode realizar diversas buscas e juntar todos os resultados para depois efetuar a extração de dados cadastrais, pesquisadores, etc. Também é útil para juntar listas de IDGPs produzidas manualmente.
ScriptGP2-cleaner é um utilitário para limpar possíveis erros em uma lista de IDGPs que tenha sido gerada manualmente ou por outra fonte que não seja o ScriptGP2. 

 
Instalação

Para executar o ScriptGP2 você irá precisar dos seguintes programas:
1.	Python 2.7 (https://www.python.org/)
2.	Navegador Firefox (https://www.mozilla.org/)
3.	Biblioteca Selenium para Python (https://pypi.python.org/pypi/selenium)

Python 2.7
O Python é uma linguagem de programação necessária para a execução dos scripts. Ela é mantida principalmente pela Python Software Foundation),  uma organização sem fins lucrativos que se dedica à linguagem de programação Python. O Python também é multiplataforma, o que significa que existem versões para Linux, Windows, MacOs, entre outros Sistemas. 
Instalando o Python 2.7.9
1.	Acesso o endereço https://www.python.org/downloads/
2.	Baixe a versão correta para o seu sistema operacional
3.	Assim que terminar de baixar o arquivo, execute-o e o Python será instalado automaticamente. 



 
ScriptGP2-busca.py   
DESCRIÇÃO 

O programa faz uma busca na base do Diretório dos Grupos de Pesquisa do CNPQ (DGP) e retorna todos as identificações (idgp) dos grupos que atendam os requisitos da busca.

É utilizado o próprio sistema de busca da base DGP (http://dgp.cnpq.br/dgp/faces/consulta/consulta_parametrizada.jsf)


CRITÉRIOS DE BUSCA

A busca é feita no nome do grupo, nome da linha de pesquisa, palavra-chave da linha de pesquisa e repercusões do grupo.

São considerados apenas os grupos certificados. Os grupos desatualizados também são incluídos na busca.


APLICAÇÃO

Este programa foi desenvolvido para auxiliar na identificacao de grupos de pesquisa através da busca por palavras-chave.


INSTALAÇÃO

O ScriptGP2-busca.py precisa dos seguintes programas
1. Python 2.7 (https://www.python.org/)
2. Navegador Firefox (https://www.mozilla.org/)
    As versões mais recentes (36 e 37(beta) ) do Firefox podem apresentar alguns problemas de compatibilidade com a biblioteca Selenium para Python.
    O problema ocorre quando o ScriptGP2-busca tenta abrir uma janela do navegador. A janela fica em branco e não processa o Script.
    Neste caso recomenda-se o uso da versão 35, que apresenta menos problemas de compatiblidade.
    Você pode obter este versão no endereço: https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/35.0/
3. Biblioteca Selenium para Python (https://pypi.python.org/pypi/selenium)


CONFIGURAÇÃO

O programa precisa de um arquivo como de configuracao e outro com os termos de busca
1 - arquivo de configuracao
    O arquivo deverá conter as seguintes linhas
    sep = \t
    arquivolistabusca = C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\lista-busca-nano.txt
    arquivosaida = C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano20150319-    
    
    
    sep é o caracter que será utilizado para separar os campos no arquivo de saída. 
        recomenda-se utilizar tabulacao \t 
    arquivolistabusca é o caminho para o arquivo com os termos de busca
    arquivosaida é o caminho para o arquivo de saida, incluindo o PREFIXO desejado para o nome do arquivo.

2 - arquivo com os termos de busca
    Este é o arquivo informa as buscas e os termos de busca a serem realizados.
    Cada linha deve conter o rótulo, o código para o tipo da busca e os termos daquela busca.
    Os termos da busca não podem exceder 100 caracteres. Caso exceda, crie uma nova linha

    Exemplo:
    busca01,2,catalysts fulereno fulerenos fullerene fullerenes grafeno graphene nanobiotecnologia nanocapsulas
    busca02,2,nanocapsules nanociência nanociências nanocomposite nanocomposites nanocompósito nanocompósitos

    Os RÓTULOS busca01 e busca02 foram criados para identificar a linha de busca. Estes rotulos nao podem conter acentos, 
    espaços em branco ou caracteres especiais como @$%, etc.

    O CÓDIGO para o tipo de pesquisa indica:
    1 - retorna os grupos que contenham TODAS as palavras de busca.
    2 - retorna os grupos que contenham QUALQUER uma das palavras de busca.
    3 - retorna os grupos que contenham a FRASE exata de busca.
    
    TERMOS DE BUSCA
    Após o código de busca, deverá ser inserida uma vírgula e em seguida devem ser colocadas as palavras de busca, separadas apenas por espaços.
    
    
    Observações
    O sistema do CNPQ não aceita aspas ou caracteres especiais como *, ?.
    Caso deseje pesquisar uma palavra composta ou frase, use o código 3.
    Exemplo
    busca05,3, nanotubos de carbono
    
    O sistema do CNPQ aceita apenas 100 caracteres por vez na expressao de busca.
    Assim, se sua expressao de busca contiver mais do que 100 caracteres você deverá dividi-la em mais linhas.
    

USO

ScriptGP2-busca.py arquivoconfig.txt

RESULTADOS

O ScriptGP2-busca.py gera dois arquivos para cada busca configurada no arquivo com os termos de busca e um arquivo de log, que 
registra cada etapa do processo de extracao.

    PREFIXO-ROTULO-idgp.list - lista com os grupos encontrados.
    PREFIXO-ROTULO-idgp-erros.list - lista com os erros econtrados e grupos que nao puderam ser identificados.
    PREFIXO-log.txt - arquivo com o registro de cada etapa do processo.

 aponte e sintetize as metas relacionadas com ações e programas para as Instituições de Ensino Superior.
