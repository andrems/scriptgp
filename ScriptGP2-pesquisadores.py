#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-pesquisadores.py
# Vers�o: 2014.09.04
#
# Fun��o: O programa recebe uma lista de idgp e retorna os pesquisadores do grupo.

#error codes used internally for the page load status
#0 - pages open correctly - successfully
#1 - server not responds or no internet connection
#2 - server responds but page or element page not found 


#=============================================================
#SETTINGS
#=============================================================

#set the tab character to be used in output
sep = '\t' 

# #arquivo de entrada com a lista de idgp
# infile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\Uninove\uninove-idgp.list"

# #caminho e prefixo para os arquivos de sa�da
# oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\Uninove\uninove-nivel1-'

import time
import re
import sys
import os.path
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
def sanitizer(input_str):
    #Removes U+FEFF character
    #The Unicode character U+FEFF is the byte order mark, or BOM, 
    #and is used to tell the difference between big- and little-endian UTF-16 encoding.
    #Can cause some problems when scrapping text from web page.
    input_str = input_str.replace(u'\ufeff', '')
    return input_str

#=============================================================
#MAIN PROGRAM
#=============================================================
arquivoConfig = sys.argv[1]
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
infile = opcoes['arquivolistaidgp']
oufile = opcoes['arquivosaida']
with open(infile, 'r') as fi:
    listaIdgp = []
    for k in fi:
        listaIdgp.append(k[:16])

listaIdgp = set(listaIdgp)
listaIdgp_achados = []
listaIdgp_naoachados = []
listaresultados=[]
listaIdgp_erros = []

#abre uma nova janela do firefox
browser = webdriver.Firefox()

for idgp in listaIdgp:
   
    #abre a p�gina do grupo
    browser.get('http://dgp.cnpq.br/dgp/espelhogrupo/'+idgp)
    
    #testa se a p�gina carregou completamente
    botoesEspelhoPesquisador = ''
    falhou = False
    try:
        #a express�o xpath carrega a lista de objetos que representam os botoes para o espelho de cada pesquisador do grupo
        botoesEspelhoPesquisador = browser.find_elements_by_xpath('/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[5]/div/fieldset/div[1]/div/table/tbody/tr[*]/td[4]/a[2]/button')
    except:
        falhou = True
   
    #se carregou corretamente
    if not falhou:
        #guarda a referencia da pagina do grupo
        paginaprincipal = browser.current_window_handle
        
        #simula o click em cada bot�o para abrir o espelho de cada pesquisador
        for botao in botoesEspelhoPesquisador:
            
            #simula o click no bot�o e testa se foi bem sucedido
            clicoubotao = True
            try:
                botao.click()
                time.sleep(2)
            except:
                clicoubotao = False
            
            #testa se o click funcinou e uma nova janela foi aberta (faz 10 tentativas)
            tentativas = 0
            abriuJanela = False
            while (not abriuJanela) and (tentativas < 10) and (clicoubotao):
                if len(browser.window_handles) <= 1:
                    print 'Esperando resposta do servidor'
                    time.sleep(10)
                    tentativas+=1
                else:
                    abriuJanela = True
            
            #se abriu a janela
            if abriuJanela:
                #muda o foco para a nova janela
                browser.switch_to_window(browser.window_handles[1]) 
                
                #tenta extrair o idlattes e o nome do pesquisador 
                sucesso = True
                try:
                    nomePesquisador = browser.find_element_by_xpath('/html/body/div[3]/div/div/div[1]/div/div[2]/div/div[1]/h1').text
                    idlattes = browser.current_url[-16:]
                
                except:
                    sucesso = False
                    
                if sucesso:
                    listaIdgp_achados.append(idgp+sep+idlattes+sep+nomePesquisador)
                    print idgp+sep+idlattes+sep+nomePesquisador
                else:
                    print 'Erro ao tentar acessar os pesquisadores do ' + idgp
                    listaIdgp_erros.append(idgp + sep + "erro ao tentar extrair dados da pagina do pesquisador")
                    erro = 2
                    
                browser.close() #close the research windows (to minimize resources usage like RAM)
                browser.switch_to_window(paginaprincipal)
            else:
                listaIdgp_erros.append(idgp + sep + "erro ao tentar abrir a pagina do pesquisador")
                browser.switch_to_window(paginaprincipal)
                
    else:
        print 'Erro ao tentar acessar o grupo ' + idgp
        listaIdgp_erros.append(idgp + sep + "erro ao tentar abrir a pagina do grupo")
    
with open(oufile+'idgp-idlattes-errors.list', 'w') as fo:
    for linhas in listaIdgp_erros:
        fo.write("%s\n" % linhas.encode('utf-8'))
        
#se o arquivo de saida j� existe, le e acrescenta aos resultados.
if os.path.isfile(oufile+'idgp-idlattes.list'):
    with open(oufile+'idgp-idlattes.list', 'r') as fo:
        for linhas in fo:
            listaIdgp_achados.append(linhas.decode('utf-8'))

#elimina possiveis registros duplicados
listaIdgp_achados = set(listaIdgp_achados)

with open(oufile+'idgp-idlattes.list', 'w') as fo:
    for linhas in listaIdgp_achados:
        fo.write("%s\n" % linhas.encode('utf-8'))
