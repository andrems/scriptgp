#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-idgp.py
# Versão: 2014.09.04
#
# Função: O programa recebe uma lista de idgp e extrai todas as informações do grupo

#error codes used internally for the page load status
#0 - pages open correctly - successfully
#1 - server not responds or no internet connection
#2 - server responds but page or element page not found 


#=============================================================
#SETTINGS
#=============================================================

#set the tab character to be used in output
sep = '\t' 

#arquivo de entrada com a lista de idgp
infile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-final-idgpcadastroerros.list"

#caminho e prefixo para os arquivos de saída
oufile = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-final-reprocessamento-"




import time
import json
import sys
import re
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
    
def sanitizer(input_str):
    input_str = input_str.replace(u'\ufeff', '')
    return input_str
    
def remove_buttons(input_str):
    input_str = input_str.replace(u'ui-button','')
    return input_str
    
def limpeza(input_str):
    input_str = sanitizer(input_str)
    input_str = remove_accents(input_str)
    input_str = remove_buttons(input_str)
    input_str = input_str.replace('\t','')
    input_str = input_str.replace('\n','')
    input_str = input_str.strip()
    return input_str
    
#the structure was initially got by command: campos = browser.find_elements_by_class_name("control-label") #find the labels
#is expected the follow structure: [lists in python starts with 0]
'''
        0 Situação do grupo:
        1 Ano de formação:
        2 Data da Situação:
        3 Data do último envio:
        4 Líder(es) do grupo:
        5 Área predominante:
        6 Instituição do grupo:
        7 Unidade:
        8 (empty)
        9 (empty)
        10 empty)
        11 Logradouro:
        12 Número:
        13 Complemento:
        14 Bairro:
        15 UF:
        16 Localidade:
        17 CEP:
        18 Caixa Postal:
        19 Latitude:
        20 Longitude:
        21 Telefone:
        22 Fax:
        23 Contato do grupo:
        24 Website:
        25 (empty)
        26 (empty)
    '''
cabecalho =  (  "idgp" + sep + 
                "nomegrupo"+ sep +
                "situacao"+ sep +
                "ano"+ sep +
                "datasituacao"+ sep +
                "dataatualizacao"+ sep +
                "lider"+ sep +
                "area"+ sep +
                "instituicao"+ sep +
                "unidade"+ sep +
                "logradouro"+ sep +
                "numero"+ sep +
                "complemento"+ sep +
                "bairro"+ sep +
                "uf"+ sep +
                "localidade"+ sep +
                "cep"+ sep +
                "caixapostal"+ sep +
                "latitude"+ sep +
                "longitude"+ sep +
                "telefone"+ sep +
                "fax"+ sep +
                "contato"+ sep +
                "website")
#=============================================================
#MAIN PROGRAM
#=============================================================
arquivoConfig = sys.argv[1]
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
infile = opcoes['arquivolistaidgp']
oufile = opcoes['arquivosaida']
with open(infile, 'r') as fi:
    listaIdgp = []
    for k in fi:
        listaIdgp.append(k[:16])
listaIdgp = set(listaIdgp)
linhas = cabecalho
listaIdgpCadastro = []
listaIdgpCadastroJson = []
listaIdgpErros = []
falhou = False
tempoinicio = time.clock()
browser = webdriver.Firefox()
for idgp in listaIdgp:
    #se falhou na tentativa anterior abre uma nova janela do browser
    if falhou:
        browser.quit()
        browser = webdriver.Firefox()
    tentativas = 1
    continua = True
    falhou = False
    while continua:
        try:
            continua = False
            browser.get('http://dgp.cnpq.br/dgp/espelhogrupo/'+idgp)
            browser.implicitly_wait(10)
            #testa se a página carregou corretamente tentando encontrar um elemento de classe com nome controls
            #se carregou corretamente este elemento deve estar presente
            browser.find_elements_by_class_name("controls")[5].text
        except:
            print 'Erro ao acessar a pagina do grupo '+idgp+' esperando 10 segundos para tentar novamente ['+str(tentativas)+' de 5]'
            browser.quit()
            time.sleep(2)
            browser = webdriver.Firefox()
            time.sleep(4)
            tentativas = tentativas + 1
            continua = True
        if tentativas == 5:
            continua = False
            falhou = True
    if not falhou:
        #Extrai identificacao e endereco
        nomeGrupo = limpeza(browser.find_element_by_xpath(".//*[@id='tituloImpressao']/h1").text)
        camposCabecalho =  browser.find_elements_by_class_name("controls")
        camposLabelCabecalho = browser.find_elements_by_class_name("control-label")
        for k in range(len(camposLabelCabecalho)):
            nomeCampo = remove_accents(camposLabelCabecalho[k].text)
            if nomeCampo == "Situacao do grupo:":
                situacaoGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Ano de formacao:":
                anoFormacaoGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Data da Situacao:":
                dataSituacaoGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Data do ultimo envio:":
                dataUltimoEnvio= limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Lider(es) do grupo:":
                liderGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Area predominante:":
                areaGrupo = limpeza(camposCabecalho[k].text.split(';')[0])
                subAreaGrupo = limpeza(camposCabecalho[k].text.split(';')[1])
            elif nomeCampo == "Instituicao do grupo:":
                instituicaoGrupo = limpeza(camposCabecalho[k].text.split(' - ')[0])
                siglaInstituicaoGrupo = limpeza(camposCabecalho[k].text.rsplit(" - ")[1])
            elif nomeCampo == "Unidade:":
                unidadeGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "UF:":
                ufGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Localidade:":
                cidadeGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "CEP:":
                cepGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Latitude:":
                latitudeGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Longitude:":
                longitudeGrupo = limpeza(camposCabecalho[k].text)
            elif nomeCampo == "Website:":
                websiteGrupo = limpeza(camposCabecalho[k].text)
            print nomeCampo + camposCabecalho[k].text
        # situacaoGrupo = limpeza(camposCabecalho[0].text)
        # anoFormacaoGrupo = limpeza(camposCabecalho[1].text)
        # dataSituacaoGrupo = limpeza(camposCabecalho[2].text)
        # dataUltimoEnvio= limpeza(camposCabecalho[3].text)
        # liderGrupo = limpeza(camposCabecalho[4].text)
        # areaGrupo = limpeza(camposCabecalho[5].text.split(';')[0])
        # subAreaGrupo = limpeza(camposCabecalho[5].text.split(';')[1])
        # instituicaoGrupo = limpeza(camposCabecalho[6].text.split(' - ')[0])
        # siglaInstituicaoGrupo = limpeza(camposCabecalho[6].text.rsplit(" - ")[1])
        # unidadeGrupo = limpeza(camposCabecalho[7].text)
        # enderecoGrupo = limpeza(camposCabecalho[11].text + ' nro:' + camposCabecalho[12].text + ' - Bairro: '+ camposCabecalho[13].text)
        # ufGrupo = limpeza(camposCabecalho[15].text)
        # cidadeGrupo = limpeza(camposCabecalho[16].text)
        # cepGrupo = limpeza(camposCabecalho[17].text)
        # latitudeGrupo = limpeza(camposCabecalho[19].text)
        # longitudeGrupo = limpeza(camposCabecalho[20].text)
        # websiteGrupo = limpeza(camposCabecalho[24].text)
            
        #repercucoes
        
        try:
            repercucoesGrupo = limpeza(browser.find_element_by_xpath(".//*[@id='repercussao']/fieldset/p").text)
            print "Extraiu repercussoes"
        except:
            repercucoesGrupo = ''
            
        #indicadores de recursos humanos
        
        nroDoutoresPesquisadores = '0'
        nroDoutoresEstudantes = '0'
        nroDoutoresTecnicos = '0'
        nroDoutoresEstrangeiros = '0'
        nroDoutoresTotal = '0'
        
        nroMestresPesquisadores = '0'
        nroMestresEstudantes = '0'
        nroMestresTecnicos = '0'
        nroMestresEstrangeiros = '0'
        nroMestresTotal = '0'
        
        nroEspecialistasPesquisadores = '0'
        nroEspecialistasEstudantes = '0'
        nroEspecialistasTecnicos = '0'
        nroEspecialistasEstrangeiros = '0'
        nroEspecialistasTotal = '0'
        
        nroOutrosPesquisadores = '0'
        nroOutrosEstudantes = '0'
        nroOutrosTecnicos = '0'
        nroOutrosEstrangeiros = '0'
        nroOutrosTotal = '0'
        
        tiposFormacao = browser.find_elements_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr[*]/td[1]")
        for k in range(1,len(tiposFormacao)+1):
            formacao = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[1]").text
            formacao = remove_accents(formacao)
            if formacao == "Doutorado":
                nroDoutoresPesquisadores = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[2]").text
                nroDoutoresEstudantes = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[3]").text
                nroDoutoresTecnicos = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[4]").text
                nroDoutoresEstrangeiros = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[5]").text
                nroDoutoresTotal = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[6]").text
            elif formacao == "Mestrado":
                nroMestresPesquisadores = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[2]").text
                nroMestresEstudantes = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[3]").text
                nroMestresTecnicos = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[4]").text
                nroMestresEstrangeiros = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[5]").text
                nroMestresTotal = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[6]").text
            elif formacao == "Especializacao":
                nroEspecialistasPesquisadores = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[2]").text
                nroEspecialistasEstudantes = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[3]").text
                nroEspecialistasTecnicos = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[4]").text
                nroEspecialistasEstrangeiros = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[5]").text
                nroEspecialistasTotal = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[6]").text
            else:
                xPesquisadores = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[2]").text
                xEstudantes = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[3]").text
                xTecnicos = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[4]").text
                xEstrangeiros = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[5]").text
                xTotal = browser.find_element_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt395_data']/tr["+str(k)+"]/td[6]").text
                nroOutrosPesquisadores = str(int(nroOutrosPesquisadores)+int(xPesquisadores))
                nroOutrosEstudantes = str(int(nroOutrosEstudantes)+int(xEstudantes))
                nroOutrosTecnicos = str(int(nroOutrosTecnicos)+int(xTecnicos))
                nroOutrosEstrangeiros = str(int(nroOutrosPesquisadores)+int(xPesquisadores))
                nroOutrosTotal = str(int(nroOutrosTotal)+int(xTotal))
        
        #linhas de pesquisa
        linhas = browser.find_elements_by_xpath(".//*[@id='idFormVisualizarGrupoPesquisa:j_idt238_data']/tr[*]/td[1]")
        #junta todas as linhas de pesquisa em um campo, separando-as internamento por ";"
        linhasPesquisaGrupo = ''.join(e.text.replace(';','')+';' for e in linhas)
        linhasPesquisaGrupo = limpeza(linhasPesquisaGrupo)
       
        #calcula o total de recursos humanos do grupo
        nroRHTot = str(int(nroDoutoresTotal)+int(nroMestresTotal)+int(nroEspecialistasTotal)+int(nroOutrosTotal))

        #junta todas as informacoes do grupo em um registro 
        listaIdgpCadastro.append(idgp + sep +
                            nomeGrupo + sep +
                            situacaoGrupo + sep +
                            anoFormacaoGrupo + sep +
                            dataSituacaoGrupo + sep +
                            dataUltimoEnvio + sep +
                            liderGrupo + sep +
                            areaGrupo + sep +
                            subAreaGrupo + sep +
                            instituicaoGrupo + sep +
                            siglaInstituicaoGrupo + sep + 
                            unidadeGrupo + sep +
                            ufGrupo + sep +
                            cidadeGrupo + sep +
                            cepGrupo + sep +
                            latitudeGrupo + sep +
                            longitudeGrupo + sep +
                            websiteGrupo + sep +
                            nroDoutoresPesquisadores + sep +
                            nroDoutoresEstudantes + sep +
                            nroDoutoresTecnicos + sep +
                            nroDoutoresEstrangeiros + sep +
                            nroDoutoresTotal + sep +
                            nroMestresPesquisadores + sep +
                            nroMestresEstudantes + sep +
                            nroMestresTecnicos + sep +
                            nroMestresEstrangeiros + sep +
                            nroMestresTotal + sep +
                            nroEspecialistasPesquisadores + sep +
                            nroEspecialistasEstudantes + sep +
                            nroEspecialistasTecnicos + sep +
                            nroEspecialistasEstrangeiros + sep +
                            nroEspecialistasTotal + sep +
                            nroOutrosPesquisadores + sep +
                            nroOutrosEstudantes + sep +
                            nroOutrosTecnicos + sep +
                            nroOutrosEstrangeiros + sep +
                            nroOutrosTotal + sep+
                            nroRHTot + sep +
                            linhasPesquisaGrupo)
        listaIdgpCadastroJson.append({
                            "Idgp": idgp,
                            "NomeGrupo" : nomeGrupo,
                            "Situacao" : situacaoGrupo,
                            "Ano" : anoFormacaoGrupo,
                            "Area" : areaGrupo,
                            "SubArea" : subAreaGrupo,
                            "NomeInstituicao" : instituicaoGrupo,
                            "SiglaInstituicao" : siglaInstituicaoGrupo,
                            "UF" : ufGrupo,
                            "Cidade" : cidadeGrupo,
                            "nroDoutPesq": nroDoutoresPesquisadores,
                            "nroDoutEst" : nroDoutoresEstudantes,
                            "nroDoutTec" : nroDoutoresTecnicos,
                            "nroDoutExt" : nroDoutoresEstrangeiros,
                            "nroDoutTot" : nroDoutoresTotal,
                            "nroMestTot" : nroMestresTotal,
                            "nroEspeTot" : nroEspecialistasTotal,
                            "nroOutrTot" : nroOutrosTotal,
                            "nroRHTot"   : nroRHTot})
        print 'Grupo: ['+idgp+'] '+nomeGrupo
        
    else:
        listaIdgpErros.append(idgp)
        print 'Erro no Grupo: ['+idgp+']'
#converte o arquivo json


#grava os arquivos de saida
with open(oufile+'idgpcadastro.list', 'w') as fo:
    for linhas in listaIdgpCadastro:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idgpcadastroerros.list', 'w') as fo:
    for linhas in listaIdgpErros:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idgpcadastro.json', 'w') as fo:
    json.dump(listaIdgpCadastroJson,fo)



