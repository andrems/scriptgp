#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Programa: ScriptGP2-join.py
# Versao: 
#
# Função: O programa recebe várias lista de idgps do diretorio de grupos de pesquisa e junta todos em
#         uma unica lista, eliminando os idgps duplicados. 
# Programador: André Santos 
#
# Uso: ScriptGP2-join.py <arquivo de configuracao)
# 
# <arquivo de configuração> 
# O arquivo de configuracao deve conter os seguintes parametros
# listadearquivos = <arquivo listas>
#                   local do arquivo que contem a lista com os arquivos a serem juntados.
# arquivosaida = nome do arquivo de saida
# exemplo (veja o ScriptGP2-join.config)
#     listadearquivos = C:\diretorio\lista-busca-nano.txt
#     arquivosaida = C:\diretorio\listaidgps-final-
# 
# <arquivo listas>
# este arquivo deve conter o caminho para cada uma das listas a serem juntadas.
# 
# exemplo:
#     C:\exemplos\nano-2015-04-02-16-idgp.list
#     C:\exemplos\outroarquivo.list
#     C:\exemplos\nano-2015-04-02-16-idgp.list
#
#=============================================================
#SETTINGS
#=============================================================

#caminho e prefixo para o arquivo de saída
#oufile = r'C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\nano-final-'
#arquivolistas = r"C:\Users\amsantos\Google Drive\GitBucked\ScriptGP\exemplos\arquivos_nano_para_juntar.txt"

import unicodedata
import sys
def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
#=============================================================
#MAIN PROGRAM
#=============================================================

#le o arquivo de configuracao
arquivoConfig = sys.argv[1]
opcoes={}
with open(arquivoConfig, 'r') as fi:
    config = []
    for k in fi:
        opcoes[k.split("=")[0].strip()] = k.split("=")[1].strip()
listatotal = []
listadearquivos = opcoes['listadearquivos']
oufile = opcoes['arquivosaida']

#le o arquivo com os arquivos de listas
with open(listadearquivos, 'r') as fi1:
    for k in fi1:
        with open(k.strip(), 'r') as fi2:
            for j in fi2:
                listatotal.append(remove_accents(j.strip().decode('UTF-8')))
                print j
listaunica = set(listatotal)
print 'Total de registros lidos...: ' + str(len(listatotal))
print 'Total de registros unicos..: ' + str(len(listaunica))
with open(oufile+'idgps-unicos.list', 'w') as fo:
    for linhas in listaunica:
        fo.write("%s\n" % linhas.encode('utf-8'))
