# Programa: ScriptGP2-idgp.py
# Versao: 2014.09.04
#
# Funcao: O programa recebe uma lista de idgp e retorna os pesquisadores do grupo.

#error codes used internally for the page load status
#0 - pages open correctly - successfully
#1 - server not responds or no internet connection
#2 - server responds but page or element page not found 


#=============================================================
#SETTINGS
#=============================================================

#set the tab character to be used in output
sep = '\t' 

#arquivo de entrada com a lista de idgp
infile = r"C:\Users\amsantos\Google Drive\artigo 170\Extracao 2015\inovacao-br-idgp.list"

#caminho e prefixo para os arquivos de saida
oufile = r'C:\Users\amsantos\Google Drive\artigo 170\Extracao 2015\inovacao-br-'

import time
import re
import unicodedata
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])
def sanitizer(input_str):
    #Removes U+FEFF character
    #The Unicode character U+FEFF is the byte order mark, or BOM, 
    #and is used to tell the difference between big- and little-endian UTF-16 encoding.
    #Can cause some problems when scrapping text from web page.
    input_str = input_str.replace(u'\ufeff', '')
    return input_str

#=============================================================
#MAIN PROGRAM
#=============================================================
with open(infile, 'r') as fi:
    listaIdgp = []
    for k in fi:
        listaIdgp.append(k[:16])

listaIdgp_achados = []
listaIdgp_naoachados = []
listaresultados=[]
listaIdgp_erros = []

#abre uma nova janela do firefox
browser = webdriver.Firefox()

for idgp in listaIdgp:
   
    #abre a pagina do grupo
    browser.get('http://dgp.cnpq.br/dgp/espelhogrupo/'+idgp)
    
    #testa se a pagina carregou completamente
    botoesInstituicoes = ''
    falhou = False
    try:
        #a expressao xpath carrega a lista de objetos que representam os botoes para a pagina das instituicoes parceiras
        botoesInstituicoes = browser.find_elements_by_xpath('/html/body/div[3]/div/div/div/div/div[2]/form/div/div[4]/span[6]/div/fieldset/div/div/table/tbody/tr[*]/td[4]/a/button')
        
    except:
        falhou = True
   
    #se carregou corretamente
    if not falhou:
        #guarda a referencia da pagina do grupo
        paginaprincipal = browser.current_window_handle
        
        #simula o click em cada botao para abrir a pagina da instituicao parceira
        for botao in botoesInstituicoes:
            
            #simula o click no botao e testa se foi bem sucedido
            clicoubotao = True
            try:
                botao.click()
                time.sleep(2)
            except:
                clicoubotao = False
            
            #testa se o click funcinou e uma nova janela foi aberta (faz 10 tentativas)
            tentativas = 0
            abriuJanela = False
            while (not abriuJanela) and (tentativas < 10) and (clicoubotao):
                if len(browser.window_handles) <= 1:
                    print 'Esperando resposta do servidor'
                    time.sleep(10)
                    tentativas+=1
                else:
                    abriuJanela = True
            
            #se abriu a janela
            if abriuJanela:
                #muda o foco para a nova janela
                browser.switch_to_window(browser.window_handles[1]) 
                
                #tenta extrair as informacoes da instituicao parceira 
                sucesso = True
                try:
                    nomeFantasiaInstituicao = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[1]/div").text
                    razaoSocialInstituicao = nome = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[2]/div").text
                    siglaInstituicao = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[3]/div").text
                    cnpjInstituicao = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[4]/div").text
                    ufInstituicao = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[5]/div").text
                    cidadeInstituicao = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[6]/div").text
                    naturezaInstituicao = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[7]/div").text
                    setorAtividade = browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[1]/div/fieldset[1]/div[9]/div").text
                    tiposRelacao = browser.find_elements_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[2]/div/div/fieldset/ul")[0].text
                    formasRemuneracao = browser.find_elements_by_xpath("/html/body/div[3]/div/div/div/div[1]/div/div[2]/div/form/div/span[3]/div/div/fieldset/ul")[0].text
               
                except:
                    sucesso = False
                    
                if sucesso:
                    #retira os caracteres de nova linha \n
                    formasRemuneracao = re.sub('\n',' :: ',formasRemuneracao)
                    tiposRelacao = re.sub('\n',' :: ',tiposRelacao)
                    listaIdgp_achados.append(
                        idgp + sep + 
                        nomeFantasiaInstituicao + sep + 
                        razaoSocialInstituicao + sep + 
                        siglaInstituicao + sep + 
                        cnpjInstituicao + sep +
                        ufInstituicao + sep +
                        cidadeInstituicao + sep +
                        naturezaInstituicao + sep +
                        setorAtividade + sep +
                        tiposRelacao + sep +
                        formasRemuneracao)
                        
                        
                    print idgp+sep+nomeFantasiaInstituicao
                else:
                    print 'Erro ao tentar acessar as instituicoes parceiras do grupo' + idgp
                    listaIdgp_erros.append(idgp + sep + "erro ao tentar extrair dados das instituicoes parceiras")
                    erro = 2
                    
                browser.close() #close the research windows (to minimize resources usage like RAM)
                browser.switch_to_window(paginaprincipal)
            else:
                listaIdgp_erros.append(idgp + sep + "erro ao tentar abrir a pagina da instituicao parceira")
                browser.switch_to_window(paginaprincipal)
                
    else:
        print 'Erro ao tentar acessar o grupo ' + idgp
        listaIdgp_erros.append(idgp + sep + "erro ao tentar abrir a pagina do grupo")
    
with open(oufile+'idgp-inst-errors.list', 'w') as fo:
    for linhas in listaIdgp_erros:
        fo.write("%s\n" % linhas.encode('utf-8'))
with open(oufile+'idgp-inst.list', 'w') as fo:
    for linhas in listaIdgp_achados:
        fo.write("%s\n" % linhas.encode('utf-8'))
